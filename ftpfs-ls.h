#ifndef __CURLFTPFS_FTPFS_LS_H__
#define __CURLFTPFS_FTPFS_LS_H__

/*
    FTP file system
    Copyright (C) 2006 Robson Braga Araujo <robsonbraga@gmail.com>

    This program can be distributed under the terms of the GNU GPL.
    See the file COPYING.
*/

#include "cache.h"

int parse_dir(const char* list, const char* dir,
              const char* name, struct stat* sbuf,
              char* linkbuf, int linklen,
              off_t h, fuse_fill_dir_t filler, void *fuse_buf);

#endif  /* __CURLFTPFS_FTPFS_LS_H__ */
