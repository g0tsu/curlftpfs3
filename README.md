### curlftpfs3

It is a fork of `curlftpfs` which works with `fuse3`.

As the FTP protocol is not very feature rich, this filesystem does not
fulfill every constraint of a real filesystem, but it should be usable
for simple tasks like copying and editing files.

### Requirements

The requirements remain the same:

```
fuse3
glib-2.0
libcurl >= 7.17.0
```


### Installation

```
./bootstrap
./configure
make
make install
```

### TODO
  While fixing the code I saw many things which require attention.

  * Fix compilation errors

### License

GPLv2
